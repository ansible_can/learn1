# Learn1

This is just an ansible learning project. This part is just to do the first step. 
Bootstrap.yml updates the hosts, creates user "ansible" and provides key for it. The "ansible" use is the user that will be used for these hosts in the future for ansible operations.


## Getting started

All hosts are linux based are either CentOS or Ubuntu.
But there are prerequisites. The root should be able to login via ssh at the beginning. This should be done passwordless
so we have to send public key.
```
ssh-copy-id -i ~/.ssh/id_rsa <192.168.0.160 -> this is host IP>
```

#  To run 
```
ansible-playbook bootstrap.yaml
```
