#!/usr/bin/expect

# Set the variables
set ssh_key_path "$env(HOME)/.ssh/id_rsa"
set password_file "pass.txt"

# Read the password from the file
set fh [open $password_file r]
set password [read $fh]
close $fh

# List of remote hosts
set remote_hosts {
    192.168.0.159
    192.168.0.160
    192.168.0.161
    192.168.0.162
}

# Loop through each remote host
foreach remote_host $remote_hosts {
    for {set i 0} {$i < 5} {incr i} {
        spawn ssh-copy-id -i $ssh_key_path $remote_host

        # Expect to see different patterns
        expect {
            "yes/no" {
                send "yes\r"
                exp_continue
            }
            "password:" {
                send "$password\r"
            }
            eof {
                break
            }
        }
    }
}

# Close the expect session
wait
